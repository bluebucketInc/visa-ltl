const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const globSync = require('glob').sync;

module.exports =  (env, options) => ({
    entry: [
        './src/index.js'
    ],
    devServer: {
        contentBase: './dist'
    },
    devtool: "source-map",
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
    ],
    resolve: {
        modules: ['node_modules'],
        alias: {
          'owl.carousel': 'owl.carousel/dist/owl.carousel.min.js'
        }
      },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [options.mode !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'img/'
                    }
                }]
            },
            // {
            //     test: /\.(woff(2)?|ttf|eot|otf|woff|svg)(\?v=\d+\.\d+\.\d+)?$/,
            //     use: [{
            //         loader: 'file-loader',
            //         options: {
            //             name: '[name].[ext]',
            //             outputPath: 'fonts/'
            //         }
            //     }]
            // },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: [':src']
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        // new CopyWebpackPlugin([
        //     { from: 'src/sass/fonts', to: 'css/fonts' }
        // ]),
        new MiniCssExtractPlugin({
            filename: "css/[name].[contenthash].css",
        }),
        new CleanWebpackPlugin(["dist"]),
        ...globSync('src/**/*.html').map((fileName) => {
            return new HtmlWebpackPlugin({
                template: fileName,
                inject: "body",
                filename: fileName.replace('src/', '')
            })
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Util: "exports-loader?Util!bootstrap/js/dist/util",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
        }),
    ],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: ''
    }
})