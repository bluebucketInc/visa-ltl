import "jquery";
import "bootstrap";
import "owl.carousel";

import "./sass/main.scss";

$(document).ready(function() {

	// carousell
	$("#participate-carousel").owlCarousel({
		items: 3,
		loop: true,
		dots: true,
		autoplay: false,
		center: 1,
		startPosition: 1,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 3
			}
		},
		nav: true
	});


	// accordion
	$(".accordion_head").click(function() {
		if ($(".accordion_body").is(":visible")) {
			$(".accordion_body").slideUp(300);
			$(".plusminus").text("+");
		}
		if ($(this).next(".accordion_body").is(":visible")) {
			$(this).next(".accordion_body").slideUp(300);
			$(this).children(".plusminus").text("+");
		} else {
			$(this).next(".accordion_body").slideDown(300);
			$(this).children(".plusminus").text("-");
		}
	});

	// mobile carousell
	if(window.innerWidth <= 768) {
		startMobileCarousel();
	}
});


// on resize
$( window ).resize(function() {
	// mobile carousell
	if(window.innerWidth <= 768) {
		startMobileCarousel();
	} else {

	}
});



function startMobileCarousel() {
	$("#pay-carousel").owlCarousel({
		items: 1,
		loop: true,
		dots: true,
		autoplay: false,
		nav: true,
	});
}